﻿using System;
using System.Data.SqlClient;
using System.Configuration;

namespace ADO.NET_Demo
{
    class DataBase
    {
        //Get a connection string from App.config
        private static string conString = ConfigurationManager.ConnectionStrings["Dbcon"].ConnectionString;

        //Make a sqlConnection
        private SqlConnection con = new SqlConnection(conString);

        public SqlConnection GetConnection()
        {
            return con;
        }

        //open a database connection
        public void OpenConnection()
        {
            if (con.State == System.Data.ConnectionState.Closed)
            {
                con.Open();
            }
        }

        //close database connection
        public void CloseConnection()
        {
            if (con.State == System.Data.ConnectionState.Open)
            {
                con.Close();
            }
        }
    }
}
