﻿using System;
using System.Data.SqlClient;

namespace ADO.NET_Demo

{
    class Program
    {
        static void Main(string[] args)
        {
            DataBase con = new DataBase();  // creating object for database class
            int choice = 0;char ch;
            do
            {
                Console.WriteLine("1.Insert data");
                Console.WriteLine("2.Select/display data");
                Console.WriteLine("3.Update data");
                Console.WriteLine("4.Delete data");
                Console.WriteLine("5.GetStudentsAll ");
                Console.WriteLine("6.based on id get student");

                Console.WriteLine("Enter your Choice");
                choice = int.Parse(Console.ReadLine());

                switch(choice)
                {
                    case 1:
                        Insert(con);
                        break;
                    case 2:
                        Select(con);
                        break;
                    case 3:
                        Update(con);
                        break;
                    case 4:
                        Delete(con);
                        break;
                    case 5:
                        GetStudentsAll(con);
                        break;
                    case 6:
                        BasedOnStudentId(con);
                        break;
                    default:
                        break;
                }
                Console.WriteLine("Do you want to Continue Y/N");
                ch = Convert.ToChar(Console.ReadLine().ToUpper());

            } while (ch=='Y');

            Console.ReadKey();
        }

        private static void BasedOnStudentId(DataBase dataBase)
        {
            dataBase.OpenConnection();
            Console.WriteLine("Enter student id to retrieve data:");
            int id = Convert.ToInt32(Console.ReadLine());

            SqlCommand sqlCommand = new SqlCommand("select * from dbo.GetStudents(@id)", dataBase.GetConnection());
            sqlCommand.Parameters.AddWithValue("@id", id);

            var dataReader = sqlCommand.ExecuteReader();
            while(dataReader.Read())
            {
                Console.WriteLine($"{dataReader.GetValue(0)} {dataReader.GetValue(1)} {dataReader.GetValue(2)}");

            }
            dataReader.Close();
            dataBase.CloseConnection();
        }

        private static void GetStudentsAll(DataBase dataBase)
        {
            dataBase.OpenConnection();

            SqlCommand sqlCommand = new SqlCommand("select * from dbo.GetStudentsAll()", dataBase.GetConnection());
            var dataReader = sqlCommand.ExecuteReader();
            while (dataReader.Read())
            {
                Console.WriteLine($"{dataReader.GetValue(0)}{dataReader.GetValue(1)}{dataReader.GetValue(2)}");

            }
            dataReader.Close();
            dataBase.CloseConnection();
        }

        public static void Insert(DataBase dataBase)
        {
            dataBase.OpenConnection(); // con.Open();

            Console.WriteLine("Enter Student Id");
            int stdId = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Enter Student name");
            int stdName = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Enter Address");
            int stdLoc = Convert.ToInt32(Console.ReadLine());
            SqlCommand cmd = new SqlCommand("Insert into Studentdetails values('" + stdId + "','" + stdName + "','" + stdLoc + "')",dataBase.GetConnection());

           int effectedRows=cmd.ExecuteNonQuery();
            if(effectedRows>0)
            Console.WriteLine(effectedRows +"are inserted");

            dataBase.CloseConnection();

            Console.WriteLine();
        }

        public static void Select(DataBase dataBase)
        {
            dataBase.OpenConnection();

            SqlCommand cmd = new SqlCommand("Select * from Studentdetails",dataBase.GetConnection());

            var dataReader=cmd.ExecuteReader();

            while(dataReader.Read())
            {
                Console.WriteLine($"{dataReader.GetValue(0)} {dataReader.GetValue(1)} {dataReader.GetValue(2)}");
                  
            }
            dataReader.Close();

            dataBase.CloseConnection();
            Console.WriteLine();
        }

        public static void Update(DataBase dataBase)
        {
            dataBase.OpenConnection();
            Console.WriteLine("Enter student id for whose you want to change name");
            int stdId = int.Parse(Console.ReadLine());

            Console.WriteLine($"Enter new Student name for StudentId {stdId}:");
            string stdName = Console.ReadLine();

            SqlCommand cmd = new SqlCommand("Update Studentdetails set stdname= '"+stdName+"' where stdid='"+stdId +"' ",dataBase.GetConnection());

            int effectedUpdatedRows=cmd.ExecuteNonQuery();
            if(effectedUpdatedRows>0)
            {
                Console.WriteLine("Updated Sucessfully"); ;
            }
            dataBase.CloseConnection(); 
            Console.WriteLine();
        }

        public static void Delete(DataBase dataBase)
        {
            dataBase.OpenConnection();
            Console.WriteLine("Enter stdId for whose you want to delete");
            int stdId = int.Parse(Console.ReadLine());

            SqlCommand cmd = new SqlCommand("Delete from  Studentdetails where stdid='"+stdId+"'",dataBase.GetConnection());

            int effectedDeletedRow=cmd.ExecuteNonQuery();

            if (effectedDeletedRow > 0)
                Console.WriteLine("deleted sucessfully");
            else
                Console.WriteLine("Record not found");
            dataBase.CloseConnection();
            Console.WriteLine();
        }
    }
}
